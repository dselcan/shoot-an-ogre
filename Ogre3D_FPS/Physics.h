/*
This class is a wrapper for the physics engine Bullet.
*/

#pragma once
#include "BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.h"
#include "BulletCollision/CollisionDispatch/btCollisionDispatcher.h"
#include "BulletCollision/BroadphaseCollision/btDbvtBroadphase.h"
#include "BulletCollision/BroadphaseCollision/btBroadphaseInterface.h"
#include "BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.h"
#include "BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h"
#include "LinearMath/btIDebugDraw.h"
#include "OgreManualObject.h"
#include "OgreSceneManager.h"
#include "OgreLogManager.h"
#include <map>
#include <vector>
#include <string>

class Physics : public btIDebugDraw
{

private:

	btDefaultCollisionConfiguration* collisionConfiguration;
	btCollisionDispatcher* dispatcher;
	btBroadphaseInterface* overlappingPairCache;
	btSequentialImpulseConstraintSolver* solver;
	btDiscreteDynamicsWorld* dynamicsWorld;
	std::vector<btCollisionShape*> collisionShapes;
	std::map<std::string, btRigidBody*> physicsAccessors;

	Ogre::SceneNode* phisycsDebuglayer;

	std::vector<Ogre::ManualObject*> debugLines;

	bool debuging;

public:

	~Physics();

	void initObjects(Ogre::SceneNode* m);

	btDiscreteDynamicsWorld* getDynamicsWorld();
	std::vector<btCollisionShape*> getCollisionShapes();
	btCollisionDispatcher* getDispatcher();
	void trackRigidBodyWithName(btRigidBody* rb, std::string name);

	int getCollisionObjectCount();

	virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& toColor);
	virtual void   drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color);

	virtual void   reportErrorWarning(const char* warningString);

	virtual void   draw3dText(const btVector3& location, const char* textString);

	virtual void   setDebugMode(int debugMode);
	virtual int    getDebugMode() const;

	void clearDebugLines();
	std::map<std::string, btRigidBody*> getPhisycsAccessors();
};

