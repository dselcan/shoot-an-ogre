#include "appSetup.h"


bool appSetup::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
    gui->getTrayManager()->frameRendered(fe);
    if (gameMgr->isPaused())//stop rendering gameplay
        return true;
    if (gameMgr->isTimeOver()) {//show game over screen
        gui->enableGameOverUI(gameMgr->getPoints());
        return true;
    }
    gui->updateTime(gameMgr->getElapsedTime());
    gui->updateScore(gameMgr->getPoints());
    player->updateAnimation(fe);
    if(!gameMgr->waitBeforeStart())//dont start spawning enemies untill player is ready
        enemyMgr->update(fe);
    return true;
}

bool appSetup::frameStarted(const Ogre::FrameEvent& fe)
{
    input->capture();
    if (gameMgr->isPaused() || gameMgr->isTimeOver())
        return true;
    if (!gameMgr->waitBeforeStart()){//show the label telling player to shhot to start
        OgreBites::Label* label = (OgreBites::Label*)gui->getTrayManager()->getWidget(GETTINGREADYLABELNAME);
        if(label)
            gui->getTrayManager()->destroyWidget(label);
    }
    physicsEngine->clearDebugLines();                   //needed when debug mode is on
    physicsEngine->getDynamicsWorld()->debugDrawWorld();//-||-
    if (this->physicsEngine != NULL) {
        physicsEngine->getDynamicsWorld()->stepSimulation(fe.timeSinceLastFrame);//calcualte snext "frame" of the physics world
        for (auto tmp : physicsEngine->getPhisycsAccessors()) {
            btRigidBody* body = tmp.second;
            if (body && body->getMotionState()) {
                btTransform trans;
                body->getMotionState()->getWorldTransform(trans); //get the new possition ov every rigid body i am keeping track of

                void* userPointer = body->getUserPointer(); //apply the new position to the actual geometry that is being drawn
                Ogre::SceneNode* sceneNode = static_cast<Ogre::SceneNode*>(userPointer);
                if (userPointer) {
                    btQuaternion orientation = trans.getRotation();
                    sceneNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
                    sceneNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
                }
            }
        }
    }
    return true;
}

void appSetup::createListeners()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("Creating Listeners");
    //register callbacks for frameStarted() & frameRenderingQueued()
    root->addFrameListener(this);
    //register UI so it renderes
    scnMgr->addRenderQueueListener(this->getOverlaySystem());
    //register input system
    addInputListener(gui->getTrayManager());
}

appSetup::appSetup() : OgreBites::ApplicationContext("Ogre3D_FPS")
{
}

void appSetup::setup()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("***************************** setup **********************************");
    //setup ogre root, scene manager & my defined classes
    OgreBites::ApplicationContext::setup();
    root = getRoot();
    scnMgr = root->createSceneManager();
    physicsEngine = new Physics();
    physicsEngine->initObjects(scnMgr->getRootSceneNode()->createChildSceneNode());
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
    Ogre::RTShader::ShaderGenerator* shadergen = Ogre::RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);
    enemyMgr = new EnemyManager(scnMgr, physicsEngine);
    gameMgr = new GameManager(enemyMgr);
    player = new Player(scnMgr, physicsEngine->getDynamicsWorld(), enemyMgr, gameMgr);
    debugCam = new DebugCamera(scnMgr);
    gui = new Gui(getRenderWindow(), gameMgr, root);
    gui->enableMainMenu();
    input = new Input(getRenderWindow(), player, debugCam, gui, gameMgr);

    //Light
    Ogre::Light* light = scnMgr->createLight("MainLight");
    Ogre::SceneNode* lightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    lightNode->setPosition(0, 10, 15);
    lightNode->attachObject(light);

    Ogre::Light* l = scnMgr->createLight();
    l->setType(Ogre::Light::LT_DIRECTIONAL);
    l->setDiffuseColour(Ogre::ColourValue::White);
    l->setSpecularColour(Ogre::ColourValue(0.4, 0.4, 0.4));
    Ogre::SceneNode* ln = scnMgr->getRootSceneNode()->createChildSceneNode();
    ln->setDirection(Ogre::Vector3(0.55, -0.3, 0.75).normalisedCopy());
    ln->attachObject(l);

    //setup camera
    getRenderWindow()->addViewport(player->getCamera());

    //prepare the level (load model and add collision information)
    Ogre::Entity* worldEntity = scnMgr->createEntity("LLWHFQP58G2RTUB3Y2EB84FVD.obj");
    Ogre::SceneNode* worldNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    worldNode->setPosition(0, 4, 0);
    worldNode->setScale(30, 30, 30);
    worldNode->attachObject(worldEntity);

    collider = new Collider(physicsEngine);
    collider->addCollider(btVector3(22, 0, 0), btVector3(0.1, 10, 38), "Rwall");
    collider->addCollider(btVector3(-22, 0, 0), btVector3(0.1, 10, 38), "Lwall");
    collider->addCollider(btVector3(0, 0, -35), btVector3(25, 10, 0.1), "Bwall");
    collider->addCollider(btVector3(-13, 0, 35), btVector3(10, 10, 0.1), "FwallL");
    collider->addCollider(btVector3(13, 0, 35), btVector3(10, 10, 0.1), "FwallR");
    
    collider->addCollider(btVector3(-9, -3.5, -19.3), btVector3(4, 1.5, 0.1), "LTomb3B");
    collider->addCollider(btVector3(-9, -3.5, -22.7), btVector3(4, 1.5, 0.1), "LTomb3F");
    collider->addCollider(btVector3(-13, -3.5, -21), btVector3(0.1, 1.5, 1.5), "LTomb3L");
    collider->addCollider(btVector3(-5, -3.5, -21), btVector3(0.1, 1.5, 1.5), "LTomb3R");

    collider->addCollider(btVector3(-9, -3.5, -4.3), btVector3(4, 1.5, 0.1), "LTomb2B");
    collider->addCollider(btVector3(-9, -3.5, -7.7), btVector3(4, 1.5, 0.1), "LTomb2F");
    collider->addCollider(btVector3(-13, -3.5, -6), btVector3(0.1, 1.5, 1.5), "LTomb2L");
    collider->addCollider(btVector3(-5, -3.5, -6), btVector3(0.1, 1.5, 1.5), "LTomb2R");

    collider->addCollider(btVector3(-9, -3.5, 10.7), btVector3(4, 1.5, 0.1), "LTomb1B");
    collider->addCollider(btVector3(-9, -3.5, 7.3), btVector3(4, 1.5, 0.1), "LTomb1F");
    collider->addCollider(btVector3(-13, -3.5, 9), btVector3(0.1, 1.5, 1.5), "LTomb1L");
    collider->addCollider(btVector3(-5, -3.5, 9), btVector3(0.1, 1.5, 1.5), "LTomb1R");

    collider->addCollider(btVector3(9, -3.5, -19.3), btVector3(4, 1.5, 0.1), "RTomb3B");
    collider->addCollider(btVector3(9, -3.5, -22.7), btVector3(4, 1.5, 0.1), "RTomb3F");
    collider->addCollider(btVector3(13, -3.5, -21), btVector3(0.1, 1.5, 1.5), "RTomb3L");
    collider->addCollider(btVector3(5, -3.5, -21), btVector3(0.1, 1.5, 1.5), "ETomb3R");

    collider->addCollider(btVector3(9, -3.5, -4.3), btVector3(4, 1.5, 0.1), "RTomb2B");
    collider->addCollider(btVector3(9, -3.5, -7.7), btVector3(4, 1.5, 0.1), "RTomb2F");
    collider->addCollider(btVector3(13, -3.5, -6), btVector3(0.1, 1.5, 1.5), "RTomb2L");
    collider->addCollider(btVector3(5, -3.5, -6), btVector3(0.1, 1.5, 1.5), "RTomb2R");

    collider->addCollider(btVector3(9, -3.5, 10.7), btVector3(4, 1.5, 0.1), "RTomb1B");
    collider->addCollider(btVector3(9, -3.5, 7.3), btVector3(4, 1.5, 0.1), "RTomb1F");
    collider->addCollider(btVector3(13, -3.5, 9), btVector3(0.1, 1.5, 1.5), "RTomb1L");
    collider->addCollider(btVector3(5, -3.5, 9), btVector3(0.1, 1.5, 1.5), "RTomb1R");

    collider->addCollider(btVector3(0, -6.1, 0), btVector3(1000, 1, 1000), "Ground");

    Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);
    Ogre::MeshPtr planePtr = Ogre::MeshManager::getSingleton().createPlane("ground", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane, 1500, 1500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);
    Ogre::Entity* entGround = scnMgr->createEntity("GroundEntity", "ground");
    Ogre::SceneNode* groundNode = scnMgr->getRootSceneNode()->createChildSceneNode("groundNode");
    groundNode->setPosition(0, -5.1, 0);
    groundNode->attachObject(entGround);

    //add players collision information to the phisycs engine
    physicsEngine->getCollisionShapes().push_back(player->getShape());
    physicsEngine->getDynamicsWorld()->addRigidBody(player->getBody());
    physicsEngine->trackRigidBodyWithName(player->getBody(), "");
    
    createListeners();
}

appSetup::~appSetup()
{
    delete player;
    delete debugCam;
    delete gameMgr;
    delete gui;
    delete input;
    delete physicsEngine;
    delete enemyMgr;
    delete collider;
    this->closeApp();
}

void appSetup::start()
{
    root->startRendering();
}
