#include "Enemy.h"

Enemy::Enemy(Ogre::SceneManager* mgr, const Ogre::Vector3& pos, const int& index)
{
    this->mgr = mgr;
    this->index = index;
	enemy = mgr->getRootSceneNode()->createChildSceneNode("Enemy" + std::to_string(index));
    Ogre::Entity* ent = mgr->createEntity("Sinbad.mesh");
    enemy->setPosition(pos);
    enemy->attachObject(ent);

    collider = new btBoxShape(btVector3(2.0f, 4.0f, 1.0f)); //I could use the Collider class instead of making a collider by hand here
    btTransform startTransform;
    startTransform.setIdentity();
    btScalar mass = 0;
    btVector3 localInertia(0, 0, 0);
    startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));
    collider->calculateLocalInertia(mass, localInertia);
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, collider, localInertia);
    body = new btRigidBody(rbInfo);
    body->setUserPointer(this);
    body->setUserIndex(index);//saves the index of enemy, so i know which one to destroy when it is hit

    //create enemy "popup" animation
    Ogre::Animation* animation = mgr->createAnimation("Enemy"+ std::to_string(index) +"Rise", 4);
    animation->setInterpolationMode(Ogre::Animation::IM_SPLINE);
    Ogre::NodeAnimationTrack* track = animation->createNodeTrack(1, enemy);
    Ogre::Vector3 scale(enemy->getScale());
    Ogre::Quaternion rotation(enemy->getOrientation());
    Ogre::Vector3 downPos(pos.x, pos.y - 7, pos.z);

    Ogre::TransformKeyFrame* key;
    key = track->createNodeKeyFrame(0);
    key->setRotation(rotation);
    key->setScale(scale);
    key->setTranslate(downPos);
    key = track->createNodeKeyFrame(0.3);
    key->setRotation(rotation);
    key->setScale(scale);
    key->setTranslate(pos);
    key = track->createNodeKeyFrame(1);
    key->setRotation(rotation);
    key->setScale(scale);
    key->setTranslate(pos);
    key = track->createNodeKeyFrame(2);
    key->setRotation(rotation);
    key->setScale(scale);
    key->setTranslate(pos);
    key = track->createNodeKeyFrame(3);
    key->setRotation(rotation);
    key->setScale(scale);
    key->setTranslate(pos);
    key = track->createNodeKeyFrame(3.7);
    key->setRotation(rotation);
    key->setScale(scale);
    key->setTranslate(pos);
    key = track->createNodeKeyFrame(4);
    key->setRotation(rotation);
    key->setScale(scale);
    key->setTranslate(downPos);

    ogreState = mgr->createAnimationState("Enemy" + std::to_string(index) + "Rise");
    ogreState->setEnabled(true);
    ogreState->setLoop(false);
}

Enemy::~Enemy()
{
    mgr->destroyAnimation("Enemy" + std::to_string(index) + "Rise");
    mgr->destroySceneNode(enemy);
    delete collider;
    delete body;
}

void Enemy::updateAnimation(const Ogre::FrameEvent& fe)
{
    ogreState->addTime(fe.timeSinceLastFrame);
    if (ogreState->getLength() == ogreState->getTimePosition()) {
        delete this;
    }
}

btCollisionShape* Enemy::getCollider()
{
    return collider;
}

btRigidBody* Enemy::getBody()
{
    return body;
}

Ogre::AnimationState* Enemy::getState()
{
    return ogreState;
}

std::string Enemy::getName()
{
    return "Enemy" + std::to_string(index);
}
