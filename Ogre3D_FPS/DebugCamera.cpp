#include "DebugCamera.h"

DebugCamera::DebugCamera(Ogre::SceneManager* sm)
{
	mgr = sm;
	cam = mgr->createCamera("debugCam");
	cam->setNearClipDistance(0.1);
	cam->setFarClipDistance(1000);
	cam->setAutoAspectRatio(true);
	debugCameraNode = mgr->getRootSceneNode()->createChildSceneNode();
	debugCameraNode->attachObject(cam);
}

void DebugCamera::move(const Ogre::Vector3& vec)
{
	debugCameraNode->translate(vec, Ogre::Node::TS_LOCAL);
}

void DebugCamera::rotate(const int& rotY, const int& rotX)
{
    float rotateRate = 0.2;
    debugPitch += Ogre::Degree(rotY * rotateRate * -1);
    debugYaw += Ogre::Degree(rotX * rotateRate * -1);
    if (debugPitch.valueDegrees() > 90)
        debugPitch = Ogre::Degree(90);
    if (debugPitch.valueDegrees() < -90)
        debugPitch = Ogre::Degree(-90);
    if (debugYaw.valueDegrees() > 360 || debugYaw.valueDegrees() < -360)
        debugYaw = Ogre::Degree(0);
    Ogre::Quaternion orientationPitch(debugPitch, Ogre::Vector3(1, 0, 0));
    Ogre::Quaternion orientationYaw(debugYaw, Ogre::Vector3(0, 1, 0));
    Ogre::Quaternion together = orientationYaw * orientationPitch;
    debugCameraNode->setOrientation(together);

}

void DebugCamera::SetAsMainCamera()
{
    mgr->getCurrentViewport()->setCamera(cam);
}
