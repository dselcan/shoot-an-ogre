#include "Input.h"
#include <Windows.h>

bool Input::keyPressed(const OIS::KeyEvent& ke)
{
    if (gameMgr->isPaused())
        return true;
    if (mKeyboard->isKeyDown(OIS::KC_F1) && debuging)
    {
        if (playerActive)
            dc->SetAsMainCamera();
        else
            player->setAsMainCamera();
        playerActive = !playerActive;
    }
    if (mKeyboard->isKeyDown(OIS::KC_ESCAPE)) {
        gameMgr->pause(true);
        gui->enablePauseMenu();
    }
    return true;
}

bool Input::keyReleased(const OIS::KeyEvent& ke)
{
    return true;
}

bool Input::mouseMoved(const OIS::MouseEvent& me)
{
    gui->getTrayManager()->mouseMoved(mouseMovedOIStoOgre(me));
    if (gameMgr->isPaused() || gameMgr->isTimeOver())
        return true;
    if (playerActive)
        player->rotate(me.state.Y.rel, me.state.X.rel);
    else
        dc->rotate(me.state.Y.rel, me.state.X.rel);
    return true;
}

bool Input::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
    if (gui->getTrayManager()->isCursorVisible()) {
        if (gui->getTrayManager()->mousePressed(mousePressedOIStoOgre(me, id)))
            return true;
    }
    if (gameMgr->isPaused() || gameMgr->isTimeOver()) {
        return true;
    }
    if (id == OIS::MouseButtonID::MB_Left) {
        player->shoot();
    }
    return true;
}

bool Input::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
    gui->getTrayManager()->mouseReleased(mousePressedOIStoOgre(me, id));
    return true;
}

//tray manager uses the OGRE input system & events to know when mouse is moved. this function transforms OIS event to OGRE event
OgreBites::MouseMotionEvent Input::mouseMovedOIStoOgre(const OIS::MouseEvent& me)
{
    OgreBites::MouseMotionEvent evt;
    evt.type = OgreBites::EventType::MOUSEMOTION;
    evt.x = me.state.X.abs;
    evt.xrel = me.state.X.rel;
    evt.y = me.state.Y.abs;
    evt.yrel = me.state.Y.rel;
    evt.windowID = me.device->getID();
    return evt;
}

//tray manager uses the OGRE input system & events to know when mouse clicks. this function transforms OIS event to OGRE event
OgreBites::MouseButtonEvent Input::mousePressedOIStoOgre(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
    OgreBites::MouseButtonEvent evt;
    evt.type = OgreBites::EventType::MOUSEBUTTONDOWN;
    evt.x = me.state.X.abs;
    evt.y = me.state.Y.abs;
    evt.button = OgreBites::ButtonType::BUTTON_LEFT;
    evt.clicks = 1;
    return evt;
}

OIS::Mouse* Input::getMouse()
{
    return mMouse;
}

OIS::Keyboard* Input::getKeyboard()
{
    return mKeyboard;
}

void Input::capture()
{
    float moveRate = 1;
    mKeyboard->capture();
    mMouse->capture();
    if (gameMgr->isPaused())
        return;
    Ogre::Vector3 move = Ogre::Vector3::ZERO;
    if (mKeyboard->isKeyDown(OIS::KC_W))
        move.z = -1 * moveRate;
    if (mKeyboard->isKeyDown(OIS::KC_S))
        move.z = 1 * moveRate;
    if (mKeyboard->isKeyDown(OIS::KC_D))
        move.x = 1 * moveRate;
    if (mKeyboard->isKeyDown(OIS::KC_A))
        move.x = -1 * moveRate;
    if (playerActive)
        player->move(move);
    else
        dc->move(move);
}

void Input::changeMouseArea(int width, int height)
{
    if (width <= 0)
        mMouse->getMouseState().width = window->getWidth();
    else
        mMouse->getMouseState().width = width;

    if (height <= 0)
        mMouse->getMouseState().height = window->getHeight();
    else
        mMouse->getMouseState().height = height;
}

Input::Input(Ogre::RenderWindow* window, Player* plr, DebugCamera* cam, Gui* gui, GameManager* mgr)
{
    debuging = false;
    player = plr;
    dc = cam;
    this->gui = gui;
    gameMgr = mgr;
    this->window = window;
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");

    OIS::ParamList pl;
    HWND windowHnd = 0;
    window->getCustomAttribute("WINDOW", &windowHnd);
    std::ostringstream windowHndStr;

    windowHndStr << (size_t)windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    mInputMgr = OIS::InputManager::createInputSystem(pl);

    mKeyboard = static_cast<OIS::Keyboard*>(
        mInputMgr->createInputObject(OIS::OISKeyboard, true));
    mMouse = static_cast<OIS::Mouse*>(
        mInputMgr->createInputObject(OIS::OISMouse, true));

    changeMouseArea();

    mKeyboard->setEventCallback(this);
    mMouse->setEventCallback(this);
}
