/*
Class that spawns a random number of enemies(1-3) at random predifened locations (6)
*/

#pragma once
#include "Physics.h"
#include "Enemy.h"

class EnemyManager
{
public:

	EnemyManager(Ogre::SceneManager* sm, Physics* engine);

	void update(const Ogre::FrameEvent& fe);
	void spawnEnemies(const std::vector<int>& positions);
	void deleteEnemy(const int &pos);
	void deleteAllEnemies();

private:
	Physics* physicsEngine;
	Ogre::SceneManager* mgr;
	std::vector<std::pair<Ogre::Vector3, Enemy*>> enemies;
};

