/*
Class for easy creation and destruction of static colliders
*/
#pragma once
#include "Physics.h"
#include <string>

#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "LinearMath/btDefaultMotionState.h"
#include "BulletDynamics/Dynamics/btRigidBody.h"

class Collider
{
public:

	Collider(Physics* p);
	void addCollider(btVector3 colliderPos, btVector3 colliderDimentions, std::string name);
	void removeCollider(std::string name);
	~Collider();


private:
	Physics* engine;
	std::map<std::string, std::pair<btBoxShape*, btRigidBody*>> colliders;
};

