/*
Class that keeps track of where in the game we are (menu, pause menu, game over...) as well as keeps track of game time & score.
*/

#pragma once
#include <chrono>
#include <ctime>
#include "EnemyManager.h"

class GameManager
{
public:
	GameManager(EnemyManager* mgr);
	bool isPaused();
	void pause(const bool& p);
	void startGame();
	void addPoints(const int& p);
	void playerIsReady();
	bool waitBeforeStart();
	std::string getElapsedTime();
	std::string getPoints();
	bool isTimeOver();
	void reset();

private:
	bool mIsPaused;
	int score;
	std::chrono::system_clock::time_point startTime;
	bool mWaitBeforeStart;
	EnemyManager* enemyMgr;
};

