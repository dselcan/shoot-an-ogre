/*
Class that creates a alternate camera, that can freely move around the world.
usefull for debugging
*/

#pragma once
#include <Ogre.h>
class DebugCamera
{

public:
	DebugCamera(Ogre::SceneManager *sm);

	void move(const Ogre::Vector3& vec);
	void rotate(const int& rotY, const int& rotX);

	void SetAsMainCamera();


private:
	Ogre::SceneManager* mgr;

	Ogre::SceneNode* debugCameraNode;

	Ogre::Camera* cam;

	Ogre::Degree debugPitch;
	Ogre::Degree debugYaw;
};

