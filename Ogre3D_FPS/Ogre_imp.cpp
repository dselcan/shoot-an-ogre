#include "Ogre_imp.h"

std::string GetExeFileName()
{
    char buffer[MAX_PATH];
    GetModuleFileNameA(NULL, buffer, MAX_PATH);
    return std::string(buffer);
}

std::string GetExePath()
{
    std::string f = GetExeFileName();
    return f.substr(0, f.find_last_of("\\/"));
}

bool Ogre_imp::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("*** main looop! ***");
    mKeyboard->capture(); //this is a hack. The correct way is to get buffered inputs from OIS, but i cant get it to work...
    mMouse->capture();    // same as above for this line
    if (mKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
        getRoot()->queueEndRendering();
    }
    Ogre::Vector3 move = Ogre::Vector3::ZERO;
    if (mKeyboard->isKeyDown(OIS::KC_W))
        move.z = -0.1;
    if (mKeyboard->isKeyDown(OIS::KC_S))
        move.z = 0.1;
    if (mKeyboard->isKeyDown(OIS::KC_D))
        move.x = 0.1;
    if (mKeyboard->isKeyDown(OIS::KC_A))
        move.x = -0.1;
    //Ogre::Quaternion rotation(Ogre::Degree(camDegYaw), Ogre::Vector3::UNIT_X); //or whatever you rotate your node by
    ///Ogre::Vector3 newDirectionVector = rotation * Ogre::Vector3(1, 0, 0); //here's your new rotated vector.
    cameraNode->translate(move , Ogre::Node::TS_LOCAL);
    if (mMouse->getMouseState().buttonDown(OIS::MB_Left)) {
        Ogre::Entity* Sphere = scnMgr->createEntity("sphere.mesh");
        Ogre::SceneNode* tmpNode = scnMgr->getRootSceneNode()->createChildSceneNode();
        tmpNode->attachObject(Sphere);
        tmpNode->setPosition(0, 0, -300);
        //Ogre::LogManager::getSingletonPtr()->logMessage("*** x:" + std::to_string(mMouse->getMouseState().width) + " y:" + std::to_string(mMouse->getMouseState().height) + " ***");
    }
    //Ogre::LogManager::getSingletonPtr()->logMessage("*** translating: " + Ogre::StringConverter().toString(move) + " ***");
    return true;
}

bool Ogre_imp::frameStarted(const Ogre::FrameEvent& fe)
{
    physicsEngine->clearDebugLines();
    physicsEngine->getDynamicsWorld()->debugDrawWorld();
    if (this->physicsEngine != NULL) {
        physicsEngine->getDynamicsWorld()->stepSimulation(1.0f / 60.0f); //suppose you have 60 frames per second
    
        for (auto tmp : physicsEngine->getPhisycsAccessors()){
            btRigidBody* body = tmp.second;
        //for (int i = 0; i < this->physicsEngine->getCollisionObjectCount(); i++) {
        //    btCollisionObject* obj = this->physicsEngine->getDynamicsWorld()->getCollisionObjectArray()[i];
        //    btRigidBody* body = btRigidBody::upcast(obj);
            if (body && body->getMotionState()) {
                btTransform trans;
                body->getMotionState()->getWorldTransform(trans);
    
                void* userPointer = body->getUserPointer();
                Ogre::SceneNode* sceneNode = static_cast<Ogre::SceneNode*>(userPointer);
                if (userPointer) {
                    btQuaternion orientation = trans.getRotation();
                    Ogre::SceneNode* sceneNode = static_cast<Ogre::SceneNode*>(userPointer);
                    sceneNode->setPosition(Ogre::Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
                    sceneNode->setOrientation(Ogre::Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
                    Ogre::LogManager::getSingletonPtr()->logMessage("*** Look Ma i'm calculating phisycs!!!! ***");
                }
            }
        }
    }
    return true;
}

bool Ogre_imp::keyPressed(const OIS::KeyEvent& ke)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("*** keyPressed ***");
    return true;
}

bool Ogre_imp::keyReleased(const OIS::KeyEvent& ke)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("*** keyReleased ***");
    return true;
}

bool Ogre_imp::mouseMoved(const OIS::MouseEvent& me)
{
    float rotateRate = 0.2;
    camDegPitch += Ogre::Degree(me.state.Y.rel * rotateRate * -1);
    camDegYaw += Ogre::Degree(me.state.X.rel * rotateRate * -1);
    if (camDegPitch.valueDegrees() > 90)
        camDegPitch = Ogre::Degree(90);
    if (camDegPitch.valueDegrees() < -90)
        camDegPitch = Ogre::Degree(-90);
    if (camDegYaw.valueDegrees() > 360 || camDegYaw.valueDegrees() < -360)
        camDegYaw = Ogre::Degree(0);
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Pitch:" + std::to_string(camDegPitch.valueDegrees()) + " Yaw: " + std::to_string(camDegYaw.valueDegrees()) + " ***");
    Ogre::Quaternion orientationPitch(camDegPitch, Ogre::Vector3(1, 0, 0));
    Ogre::Quaternion orientationYaw(camDegYaw, Ogre::Vector3(0, 1, 0));
    cameraNode->setOrientation(orientationYaw);
    cameraPitchNode->setOrientation(orientationPitch);
    return true;
}

bool Ogre_imp::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
    return true;
}

bool Ogre_imp::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
    return true;
}

Ogre_imp::Ogre_imp() : OgreBites::ApplicationContext("Ogre3D_FPS")
{
    physicsEngine = new Physics();
    //physicsEngine->setDebugMode(1);
}

void Ogre_imp::setup()
{
    // do not forget to call the base first
    OgreBites::ApplicationContext::setup();

    // get a pointer to the already created root
    Ogre::Root* root = getRoot();
    scnMgr = root->createSceneManager();
    physicsEngine->initObjects(scnMgr->getRootSceneNode()->createChildSceneNode());
    //mWindow = root->initialise(false, "Ogre3D_FPS Window");

    // register our scene with the RTSS
    Ogre::RTShader::ShaderGenerator* shadergen = Ogre::RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);

    // without light we would just get a black screen    
    Ogre::Light* light = scnMgr->createLight("MainLight");
    Ogre::SceneNode* lightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    lightNode->setPosition(0, 10, 15);
    lightNode->attachObject(light);

    Ogre::Light* l = scnMgr->createLight();
    l->setType(Ogre::Light::LT_DIRECTIONAL);
    l->setDiffuseColour(Ogre::ColourValue::White);
    l->setSpecularColour(Ogre::ColourValue(0.4, 0.4, 0.4));

    Ogre::SceneNode* ln = scnMgr->getRootSceneNode()->createChildSceneNode();
    ln->setDirection(Ogre::Vector3(0.55, -0.3, 0.75).normalisedCopy());
    ln->attachObject(l);


    // create the camera
    cam = scnMgr->createCamera("myCam");
    cam->setNearClipDistance(0.1);
    cam->setFarClipDistance(1000);
    cam->setAutoAspectRatio(true);

    createFrameListener();

    // and tell it to render into the main window
    getRenderWindow()->addViewport(cam);

    // finally something to render
    //Ogre::Entity* ent = scnMgr->createEntity("Sinbad.mesh");
    //sinbad = scnMgr->getRootSceneNode()->createChildSceneNode();
    //sinbad->setPosition(0, 0, 0);
    //sinbad->attachObject(ent);

    //load our "world" model
    //Ogre::AssimpLoader loader;
    //Ogre::Mesh* world = nullptr;
    //Ogre::MeshPtr Mesh = Ogre::MeshManager::getSingleton().createManual("GraveMesh", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
    //Ogre::SkeletonPtr ptr = nullptr;
    //std::string path = GetExePath() + "\\Grave\\LLWHFQP58G2RTUB3Y2EB84FVD.obj";
    //loader.load(path, Mesh.get(), ptr);
    //Ogre::Entity* worldEntity = scnMgr->createEntity(Mesh);
    Ogre::Entity* worldEntity = scnMgr->createEntity("LLWHFQP58G2RTUB3Y2EB84FVD.obj");
    Ogre::SceneNode* worldNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    worldNode->setPosition(0, 0, 0);
    worldNode->setScale(30, 30, 30);
    worldNode->attachObject(worldEntity);

    Ogre::Plane plane(Ogre::Vector3::UNIT_Y, 0);
    Ogre::MeshPtr planePtr = Ogre::MeshManager::getSingleton().createPlane("ground", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, plane, 1500, 1500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);

    Ogre::Entity* entGround = scnMgr->createEntity("GroundEntity", "ground");
    Ogre::SceneNode* groundNode = scnMgr->getRootSceneNode()->createChildSceneNode("groundNode");

    groundNode->attachObject(entGround);

    //create the plane entity to the physics engine, and attach it to the node

    btTransform groundTransform;
    groundTransform.setIdentity();
    groundTransform.setOrigin(btVector3(0, -50, 0));

    btScalar groundMass(0.); //the mass is 0, because the ground is immovable (static)
    btVector3 localGroundInertia(0, 0, 0);

    btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(50.), btScalar(50.), btScalar(50.)));
    btDefaultMotionState* groundMotionState = new btDefaultMotionState(groundTransform);

    groundShape->calculateLocalInertia(groundMass, localGroundInertia);

    btRigidBody::btRigidBodyConstructionInfo groundRBInfo(groundMass, groundMotionState, groundShape, localGroundInertia);
    btRigidBody* groundBody = new btRigidBody(groundRBInfo);

    //add the body to the dynamics world
    this->physicsEngine->getDynamicsWorld()->addRigidBody(groundBody);

    //Ogre::MeshPtr mesh = Ogre::MeshManager::getSingleton().getByName("Cube.mesh").staticCast<Ogre::Mesh>();

    Ogre::Entity* entity = this->scnMgr->createEntity("Sinbad.mesh");

    Ogre::SceneNode* newNode = this->scnMgr->getRootSceneNode()->createChildSceneNode("physicsCubeName");
    newNode->setPosition(0, 2, 0);
    newNode->attachObject(entity);

    //create the new shape, and tell the physics that is a Box
    btCollisionShape* newRigidShape = new btBoxShape(btVector3(1.0f, 4.0f, 1.0f));
    this->physicsEngine->getCollisionShapes().push_back(newRigidShape);

    //set the initial position and transform. For this demo, we set the tranform to be none
    btTransform startTransform;
    startTransform.setIdentity();
    //startTransform.setRotation(btQuaternion(0, 0, 0, 0));

    //set the mass of the object. a mass of "0" means that it is an immovable object
    btScalar mass = 0.0f;
    btVector3 localInertia(0, 0, 0);

    startTransform.setOrigin(btVector3(0, 0, 0));
    newRigidShape->calculateLocalInertia(mass, localInertia);

    //actually contruvc the body and add it to the dynamics world
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);

    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, newRigidShape, localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);
    body->setRestitution(1);
    body->setUserPointer(newNode);

    physicsEngine->getDynamicsWorld()->addRigidBody(body);
    physicsEngine->trackRigidBodyWithName(body, "physicsCubeName");

}


void Ogre_imp::createFrameListener()
{

    // Create the camera's top node (which will only handle position).
    cameraNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    cameraNode->setPosition(0, 0, 15);
    cameraNode->lookAt(Ogre::Vector3(0, 0, -1), Ogre::Node::TS_PARENT);

    // Create the camera's pitch node as a child of camera's yaw node.
    cameraPitchNode = cameraNode->createChildSceneNode();
    cameraPitchNode->attachObject(cam);

    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");

    OIS::ParamList pl;
    HWND windowHnd = 0;
    getRenderWindow()->getCustomAttribute("WINDOW", &windowHnd);
    std::ostringstream windowHndStr;

    windowHndStr << (size_t)windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    mInputMgr = OIS::InputManager::createInputSystem(pl);

    mKeyboard = static_cast<OIS::Keyboard*>(
        mInputMgr->createInputObject(OIS::OISKeyboard, true));
    mMouse = static_cast<OIS::Mouse*>(
        mInputMgr->createInputObject(OIS::OISMouse, true));

    mKeyboard->setEventCallback(this);
    mMouse->setEventCallback(this);

    //windowResized(getRenderWindow());

    //OgreBites::WindowEventUtilities::addWindowEventListener(getRenderWindow(), this);

    getRoot()->addFrameListener(this);

    Ogre::LogManager::getSingletonPtr()->logMessage("Finished");
}

void Ogre_imp::windowResized(Ogre::RenderWindow* rw)
{
    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState& ms = mMouse->getMouseState();
    ms.width = width;
    ms.height = height;
}
