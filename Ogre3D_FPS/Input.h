/*
Class that tracks what inputs are being pressed.
*/

#pragma once
#include <Ogre.h>
#include <OgreInput.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>
#include "Player.h"
#include "DebugCamera.h"
#include "Gui.h"
#include "GameManager.h"

class Input : public OIS::KeyListener, public OIS::MouseListener
{
private:

    bool debuging;

    OIS::Mouse* mMouse;
    OIS::Keyboard* mKeyboard;
    OIS::InputManager* mInputMgr;

    Ogre::RenderWindow* window;

    Player* player;
    DebugCamera* dc;
    GameManager* gameMgr;

    Gui* gui;

    bool playerActive = true;
    bool* isPaused;

    bool keyPressed(const OIS::KeyEvent& ke);
    bool keyReleased(const OIS::KeyEvent& ke);

    bool mouseMoved(const OIS::MouseEvent& me);
    bool mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id);
    bool mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id);

    OgreBites::MouseMotionEvent mouseMovedOIStoOgre(const OIS::MouseEvent& me);
    OgreBites::MouseButtonEvent mousePressedOIStoOgre(const OIS::MouseEvent& me, OIS::MouseButtonID id);

public:

    OIS::Mouse* getMouse();
    OIS::Keyboard* getKeyboard();

    void capture();

    void changeMouseArea(int width = 0, int height = 0);

    Input(Ogre::RenderWindow* window, Player* plr, DebugCamera* cam, Gui* gui, GameManager* mgr);
};

