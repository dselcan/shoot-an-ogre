/*
Class that sets up the whole scene + callbacks for game loop
*/
#pragma once

#include "Ogre.h"
#include "OgreApplicationContext.h"

#include "input.h"
#include "player.h"
#include "DebugCamera.h"
#include "EnemyManager.h"
#include "Gui.h"
#include "GameManager.h"
#include "Collider.h"

#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "LinearMath/btDefaultMotionState.h"

class appSetup : public OgreBites::ApplicationContext
{

private:
	bool frameRenderingQueued(const Ogre::FrameEvent& fe);
	bool frameStarted(const Ogre::FrameEvent& fe);
	void createListeners();

	Ogre::Root* root;
	Ogre::SceneManager* scnMgr;
	Player* player;
	DebugCamera* debugCam;
	Input* input;

	EnemyManager* enemyMgr;
	GameManager* gameMgr;

	Physics* physicsEngine;
	Collider* collider;

	Gui* gui;

public:
	appSetup();
	~appSetup();

	void start();
	void setup();
};

