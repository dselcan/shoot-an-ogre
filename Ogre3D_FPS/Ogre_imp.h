#pragma once

#include "Ogre.h"
#include "OgreApplicationContext.h"
#include "OgreFrameListener.h"
#include "OgreCameraMan.h"
#include "OgreWindowEventUtilities.h"

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include "OgreAssimpLoader.h"

#include "LinearMath/btTransform.h"
#include "LinearMath/btDefaultMotionState.h"
#include "BulletCollision/CollisionShapes/btCollisionShape.h"
#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "BulletDynamics/Dynamics/btRigidBody.h"

#include "Physics.h"

#include "windows.h"
#include <string>


class Ogre_imp : public OgreBites::ApplicationContext, public OIS::KeyListener, OIS::MouseListener
{

private:
    Ogre::SceneNode* sinbad;
    //Ogre::Entity* Sphere;
    Ogre::SceneManager* scnMgr;
    //Ogre::RenderWindow* mWindow;

    OIS::Mouse* mMouse;
    OIS::Keyboard* mKeyboard;
    OIS::InputManager* mInputMgr;

    Ogre::SceneNode* cameraNode;
    Ogre::SceneNode* cameraPitchNode;
    Ogre::Camera* cam;

    Ogre::Degree camDegPitch;
    Ogre::Degree camDegYaw;

    Physics* physicsEngine;

    virtual bool frameRenderingQueued(const Ogre::FrameEvent& fe);

    virtual bool frameStarted(const Ogre::FrameEvent& fe);

    virtual bool keyPressed(const OIS::KeyEvent& ke);
    virtual bool keyReleased(const OIS::KeyEvent& ke);

    virtual bool mouseMoved(const OIS::MouseEvent& me);
    virtual bool mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id);
    virtual bool mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id);

    virtual void windowResized(Ogre::RenderWindow* rw);

    void createFrameListener();

public:
    Ogre_imp();
    void setup();
};