#include "GameManager.h"
#include <iostream>

GameManager::GameManager(EnemyManager* mgr)
{
    enemyMgr = mgr;
    mIsPaused = true;
    startTime = std::chrono::system_clock::time_point().min();
    score = 0;
    mWaitBeforeStart = true;
}

bool GameManager::isPaused()
{
    return mIsPaused;
}

void GameManager::pause(const bool& p)
{
    mIsPaused = p;
}

void GameManager::startGame()
{
    pause(false);
    score = 0;
    mWaitBeforeStart = true;
    enemyMgr->deleteAllEnemies();
    startTime = std::chrono::system_clock::time_point().min();
}

void GameManager::addPoints(const int& p)
{
    score += p;
}

void GameManager::playerIsReady()
{
    startTime = std::chrono::system_clock::now() + std::chrono::minutes(1);
    mWaitBeforeStart = false;
}

bool GameManager::waitBeforeStart()
{
    return mWaitBeforeStart;
}

std::string GameManager::getElapsedTime()
{
    if (startTime == std::chrono::system_clock::time_point().min()) {
        return "01:00";
    }
    time_t remaining = std::chrono::system_clock::to_time_t(startTime) - std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    if (remaining <= 0) {
        return "00:00";
    }
    struct tm* ptm = localtime(&remaining);
    char str[80];

    std::strftime(str, 80, "%M:%S", ptm);//timeinfo);
    return std::string(str);
}

std::string GameManager::getPoints()
{
    return std::to_string(score);
}

bool GameManager::isTimeOver()
{
    if (startTime == std::chrono::system_clock::time_point().min())
        return false;
    time_t remainig = std::chrono::system_clock::to_time_t(startTime) - std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    if (remainig <= 0) {
        return true;
    }
    return false;
}

void GameManager::reset()
{
    mIsPaused = true;
    startTime = std::chrono::system_clock::time_point().min();
    score = 0;
    mWaitBeforeStart = true;
}