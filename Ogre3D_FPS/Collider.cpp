#include "Collider.h"

Collider::Collider(Physics* p)
{
	engine = p;
}

void Collider::addCollider(btVector3 colliderPos, btVector3 colliderDimentions, std::string name)
{
    btBoxShape* collider = new btBoxShape(colliderDimentions);
    btTransform startTransform;
    startTransform.setIdentity();
    btScalar mass = 0;
    btVector3 localInertia(0, 0, 0);
    startTransform.setOrigin(colliderPos);
    collider->calculateLocalInertia(mass, localInertia);
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, collider, localInertia);
    btRigidBody* body = new btRigidBody(rbInfo);
    engine->getCollisionShapes().push_back(collider);
    engine->getDynamicsWorld()->addRigidBody(body);
}

void Collider::removeCollider(std::string name)
{
    std::pair<btBoxShape*, btRigidBody*> col = colliders.find(name)->second;
    engine->getDynamicsWorld()->removeRigidBody(col.second);
    delete col.first;
    delete col.second;
}

Collider::~Collider()
{
    std::map < std::string, std::pair<btBoxShape*, btRigidBody*>>::iterator it = colliders.begin();
    for (; it != colliders.end(); it++) {
        removeCollider(it->first);
    }
}
