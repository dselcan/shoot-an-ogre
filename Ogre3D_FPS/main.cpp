#include "appSetup.h"

int main() {
    appSetup app;
    app.initApp();
    app.start();
    return 0;
}
