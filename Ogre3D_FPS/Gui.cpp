#include "Gui.h"

Gui::Gui(Ogre::RenderWindow* window, GameManager* mgr, Ogre::Root* root)
{
	this->root = root;
	mGameMgr = mgr;
	mTrayMgr = new OgreBites::TrayManager("TrayManager", window, this);
	mTrayMgr->hideAll();
}

Gui::~Gui()
{
	delete mTrayMgr;
}

void Gui::enableMainMenu()
{
	mTrayMgr->destroyAllWidgets();
	mTrayMgr->createButton(OgreBites::TL_CENTER, PLAYBUTTONNAME, "Play", 200);
	mTrayMgr->createButton(OgreBites::TL_CENTER, QUITBUTTONNAME, "Quit Game", 200);
	mTrayMgr->showBackdrop("Backdrop");
	mTrayMgr->showAll();
}

void Gui::enablePauseMenu()
{
	mTrayMgr->destroyAllWidgets();
	mTrayMgr->createButton(OgreBites::TL_CENTER, CONTINUEBUTTONNAME, "Continue", 200);
	mTrayMgr->createButton(OgreBites::TL_CENTER, EXITBUTTONNAME, "Exit to Menu", 200);
	mTrayMgr->showAll();
	mTrayMgr->hideBackdrop();
}

void Gui::enableGamePlayUi()
{
	mTrayMgr->destroyAllWidgets();
	mTrayMgr->createDecorWidget(OgreBites::TL_CENTER, "CrosshairPanel", "CrosshairOverlay");
	mTrayMgr->createLabel(OgreBites::TL_TOPLEFT, SCORELABELNAME, "0", 100);
	mTrayMgr->createLabel(OgreBites::TL_TOP, TIMELABELNAME, "01:00", 100);
	mTrayMgr->createLabel(OgreBites::TL_BOTTOM, GETTINGREADYLABELNAME, "Shoot once to start the timer.", 300);
	mTrayMgr->showAll();
	mTrayMgr->hideBackdrop();
	mTrayMgr->hideCursor();
}

void Gui::enableGameOverUI(const std::string& score)
{
	OgreBites::Label* lbl = (OgreBites::Label*)mTrayMgr->getWidget(FINALSCORELABELNAME);
	if (lbl)
		return;
	mTrayMgr->destroyAllWidgets();
	mTrayMgr->createLabel(OgreBites::TL_CENTER, FINALSCORELABELNAME, "Final Score: " + score, 200);
	mTrayMgr->createButton(OgreBites::TL_CENTER, PLAYAGAINBUTTONNAME, "Play Again", 200);
	mTrayMgr->createButton(OgreBites::TL_CENTER, EXITBUTTONNAME, "Exit to Menu", 200);
	mTrayMgr->showAll();
	mTrayMgr->hideBackdrop();
}

OgreBites::TrayManager* Gui::getTrayManager()
{
	return mTrayMgr;
}


void Gui::buttonHit(OgreBites::Button* button)
{
	if (button->getName() == CONTINUEBUTTONNAME) {
		enableGamePlayUi();
		mGameMgr->pause(false);
	}
	else if (button->getName() == PLAYBUTTONNAME) {
		enableGamePlayUi();
		mGameMgr->startGame();
	}
	else if (button->getName() == EXITBUTTONNAME) {
		mGameMgr->reset();
		enableMainMenu();
	}
	else if (button->getName() == QUITBUTTONNAME) {
		root->queueEndRendering();
	}
	else if (button->getName() == PLAYAGAINBUTTONNAME) {
		enableGamePlayUi();
		mGameMgr->startGame();
	}
}

void Gui::updateScore(const std::string& score)
{
	OgreBites::Label* lbl = (OgreBites::Label*)mTrayMgr->getWidget(SCORELABELNAME);
	if (lbl)
		lbl->setCaption(score);
}

void Gui::updateTime(const std::string& time)
{
	OgreBites::Label* lbl = (OgreBites::Label*)mTrayMgr->getWidget(TIMELABELNAME);
	if (lbl)
		lbl->setCaption(time);
}
