/*
Class that creates and controls the player (moving, shooting, ...)
*/

#pragma once

#include <Ogre.h>
#include "Physics.h"
#include "EnemyManager.h"
#include "GameManager.h"
#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "BulletDynamics/Dynamics/btRigidBody.h"
#include "BulletCollision/CollisionShapes/btCollisionShape.h"
#include "LinearMath/btDefaultMotionState.h"


class Player
{

public:
    Player(Ogre::SceneManager* mgr, btDynamicsWorld* world, EnemyManager* EnemyMgr, GameManager* gMgr);

    void move(const Ogre::Vector3& moveVec);
    void rotate(const int& rotY, const int& rotX);
    void shoot(); //TODO: needs implementation
    void updateAnimation(const Ogre::FrameEvent& fe);
    void setAsMainCamera();

    btRigidBody* getBody();
    btCollisionShape* getShape();

    Ogre::AnimationState* getAnimState();

    Ogre::Camera* getCamera();

private:

    Ogre::SceneNode* playerNode;
    Ogre::SceneNode* cameraNode;
    Ogre::SceneNode* cameraPitchNode;
    Ogre::SceneNode* handsNode;
    Ogre::Camera* cam;
    Ogre::AnimationState* shootAnimationState;
    Ogre::SceneManager* sm;

    btRigidBody* body;
    btCollisionShape* shape;
    btDynamicsWorld* physicsWorld;

    Ogre::Degree camDegPitch;
    Ogre::Degree camDegYaw;
    bool animationPlaying;

    EnemyManager* enemyMgr;
    GameManager* gameMgr;

    bool debuging;
};

