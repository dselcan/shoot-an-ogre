/*
Class that manages the GUI of the game. Creating and destroying elements as needed
*/

#pragma once
#include <Ogre.h>
#include "OgreTrays.h"
#include "GameManager.h"

static std::string CONTINUEBUTTONNAME = "ContinueButton";
static std::string PLAYBUTTONNAME = "PlayButton";
static std::string QUITBUTTONNAME = "QuitButton";
static std::string EXITBUTTONNAME = "ExitButton";
static std::string TIMELABELNAME = "TimeLabel";
static std::string SCORELABELNAME = "ScoreLabel";
static std::string PLAYAGAINBUTTONNAME = "PlayAgainButton";
static std::string FINALSCORELABELNAME = "FinalScoreLabel";
static std::string GETTINGREADYLABELNAME = "GettingReadyLabel";

class Gui : public OgreBites::TrayListener
{

public:

	Gui(Ogre::RenderWindow* window, GameManager* mgr, Ogre::Root* root);
	~Gui();
	void enableMainMenu();
	void enablePauseMenu();
	void enableGamePlayUi();
	void enableGameOverUI(const std::string &score);

	OgreBites::TrayManager* getTrayManager();

	void buttonHit(OgreBites::Button* button);

	void updateTime(const std::string& time);
	void updateScore(const std::string& score);

private:

	OgreBites::TrayManager* mTrayMgr;

	GameManager* mGameMgr;
	Ogre::Root* root;
};

