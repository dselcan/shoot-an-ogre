#include "Player.h"

Player::Player(Ogre::SceneManager* mgr, btDynamicsWorld* world, EnemyManager* EnemyMgr, GameManager* gMgr)
{
    debuging = false;
    sm = mgr;
    physicsWorld = world;
    enemyMgr = EnemyMgr;
    gameMgr = gMgr;
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing Camera ***");
    //setup camera and nodes
    Ogre::Vector3 startPos(0, 2, 30);
    cam = sm->createCamera("myCam");
    cam->setNearClipDistance(0.1);
    cam->setFarClipDistance(1000);
    cam->setAutoAspectRatio(true);
    playerNode = sm->getRootSceneNode()->createChildSceneNode("PlayerNode");
    playerNode->setPosition(startPos);
    playerNode->lookAt(Ogre::Vector3(0, 0, -1), Ogre::Node::TS_PARENT);
    cameraNode = playerNode->createChildSceneNode("cameraNode");
    cameraNode->translate(0, 3, 0);
    cameraPitchNode = cameraNode->createChildSceneNode();
    cameraPitchNode->attachObject(cam);

    //setup hands model
    Ogre::Entity* hnd = sm->createEntity("e1c99d086b684e58832ce5b6a69cd912.fbx.fbx");
    Ogre::Vector3 handsScale(0.05, 0.05, 0.05);
    Ogre::Vector3 moveHands(0, -2, -1);
    handsNode = cameraPitchNode->createChildSceneNode();
    handsNode->lookAt(Ogre::Vector3(0, 0, 1), Ogre::Node::TS_PARENT);
    handsNode->translate(moveHands);
    handsNode->setScale(handsScale);
    handsNode->attachObject(hnd);
    
    //setup shooting animation
    Ogre::Animation* animation = sm->createAnimation("ShootAnim", 0.3);
    animation->setInterpolationMode(Ogre::Animation::IM_SPLINE);
    Ogre::NodeAnimationTrack* track = animation->createNodeTrack(0, handsNode);
    Ogre::Quaternion startRotation = Ogre::Quaternion(Ogre::Degree(0), Ogre::Vector3(1, 0, 0)) * Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3(0, 1, 0));
    Ogre::Quaternion midRotation = Ogre::Quaternion(Ogre::Degree(25), Ogre::Vector3(1, 0, 0)) * Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3(0, 1, 0));

    Ogre::TransformKeyFrame* key;
    key = track->createNodeKeyFrame(0);
    key->setRotation(startRotation);
    key->setScale(handsScale);
    key->setTranslate(moveHands);
    key = track->createNodeKeyFrame(0.1);
    key->setRotation(midRotation);
    key->setScale(handsScale);
    key->setTranslate(moveHands);
    key = track->createNodeKeyFrame(0.3);
    key->setRotation(startRotation);
    key->setScale(handsScale);
    key->setTranslate(moveHands);
    
    shootAnimationState = sm->createAnimationState("ShootAnim");
    shootAnimationState->setEnabled(false);
    shootAnimationState->setLoop(false);
    animationPlaying = false;

    //create collider and rigidbody info
    shape = new btBoxShape(btVector3(1.0f, 4.0f, 1.0f));
    btTransform startTransform;
    startTransform.setIdentity();
    btScalar mass = 0.1f;
    btVector3 localInertia(0, 0, 0);
    startTransform.setOrigin(btVector3(startPos.x, 0, startPos.z));
    shape->calculateLocalInertia(mass, localInertia);
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, shape, localInertia);
    body = new btRigidBody(rbInfo);
    body->setAngularFactor(btVector3(0, 0, 0));//do not rotate the rigid body via physics... dont whant the player to topple over
    body->setLinearFactor(btVector3(1, 1, 1));//player can move in all three directions (walking WASD & up down gravity)
    body->setRestitution(1);
    body->setUserPointer(playerNode);
}

void Player::move(const Ogre::Vector3& moveVec)
{
    float speed = 15;
    Ogre::Vector3 mv = playerNode->getOrientation() * moveVec.normalisedCopy();
    btTransform t = body->getWorldTransform();
    btVector3 v = t.getOrigin();
    t.setOrigin(btVector3(v.x() + mv.x, v.y() + mv.y, v.z() + mv.z));
    body->activate(true);
    body->setLinearVelocity(btVector3(mv.x * speed, body->getLinearVelocity().y(), mv.z * speed));
}

void Player::rotate(const int& rotY, const int& rotX)
{
    float rotateRate = 0.2;
    camDegPitch += Ogre::Degree(rotY * rotateRate * -1);
    camDegYaw += Ogre::Degree(rotX * rotateRate * -1);
    if (camDegPitch.valueDegrees() > 90)
        camDegPitch = Ogre::Degree(90);
    if (camDegPitch.valueDegrees() < -90)
        camDegPitch = Ogre::Degree(-90);
    if (camDegYaw.valueDegrees() > 360 || camDegYaw.valueDegrees() < -360)
        camDegYaw = Ogre::Degree(0);
    Ogre::Quaternion orientationPitch(camDegPitch, Ogre::Vector3(1, 0, 0));
    Ogre::Quaternion orientationYaw(camDegYaw, Ogre::Vector3(0, 1, 0));
    cameraPitchNode->setOrientation(orientationPitch);
    body->getWorldTransform().setRotation(btQuaternion(btVector3(0, 1, 0), camDegYaw.valueRadians()));
}

void Player::shoot()
{
    if (animationPlaying) {//dont shoot angin if the animation isnt done playing
        return;
    }
    if (gameMgr->waitBeforeStart())//player has shot the first time. ready to spawn enemies
        gameMgr->playerIsReady();
    shootAnimationState->setLoop(false);
    shootAnimationState->setEnabled(true);
    animationPlaying = true;
    btVector3 btFrom(cameraNode->_getDerivedPosition().x, cameraNode->_getDerivedPosition().y, cameraNode->_getDerivedPosition().z);//shoot from vector
    Ogre::Quaternion mixed = cameraPitchNode->_getDerivedOrientation(); //vector that descripbes where the player is looking
    
    Ogre::Vector3 v = mixed * Ogre::Vector3::NEGATIVE_UNIT_Z * 100; //vector that is facing the same way the player is facing as is extended by a thousand units
    v = Ogre::Vector3(v.x + playerNode->getPosition().x, v.y + cameraNode->getPosition().y, v.z + playerNode->getPosition().z); //add the player position because without this the endpoint is always calculated with the starting point (0, 0, 0)
    btVector3 btTo(v.x, v.y, v.z); 
    btCollisionWorld::ClosestRayResultCallback res(btFrom, btTo);
    physicsWorld->rayTest(btFrom, btTo, res); //cast a ray from player to where he is facing

    if (debuging) { //draw a line of the ray
        Ogre::ManualObject* mDragLine = new Ogre::ManualObject("Line");
        mDragLine->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST);
        mDragLine->position(cameraNode->_getDerivedPosition());
        mDragLine->position(v);
        mDragLine->end();
        Ogre::SceneNode* node = sm->getRootSceneNode()->createChildSceneNode();
        node->attachObject(mDragLine);
    }
    
    if (res.hasHit()) {//if the ray has hit something
        Enemy* n = (Enemy*)res.m_collisionObject->getUserPointer(); //check that we hit a enemy
        int num = res.m_collisionObject->getUserIndex(); //only eneies have userIndex defined
        if (n) {
            enemyMgr->deleteEnemy(num);
            gameMgr->addPoints(50);
        }
    }
}

void Player::updateAnimation(const Ogre::FrameEvent& fe)
{
    if (animationPlaying)
        shootAnimationState->addTime(fe.timeSinceLastFrame);
    if (shootAnimationState->getLength() == shootAnimationState->getTimePosition()) {
        shootAnimationState->setTimePosition(0);
        shootAnimationState->setEnabled(false);
        animationPlaying = false;
    }
}

void Player::setAsMainCamera()
{
    sm->getCurrentViewport()->setCamera(cam);
}

btRigidBody* Player::getBody()
{
    return body;
}

btCollisionShape* Player::getShape()
{
    return shape;
}

Ogre::AnimationState* Player::getAnimState()
{
    return shootAnimationState;
}

Ogre::Camera* Player::getCamera()
{
    return cam;
}

