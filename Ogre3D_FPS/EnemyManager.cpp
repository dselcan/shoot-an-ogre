#include "EnemyManager.h"
#include <stdlib.h>

EnemyManager::EnemyManager(Ogre::SceneManager* sm, Physics* engine)
{
	mgr = sm;
	physicsEngine = engine;
	enemies = { 
		{Ogre::Vector3(-9, 0, -21), nullptr},
		{Ogre::Vector3(9, 0, -21), nullptr},
		{Ogre::Vector3(-9, 0, -6), nullptr},
		{Ogre::Vector3(9, 0, -6), nullptr},
		{Ogre::Vector3(-9, 0, 9), nullptr},
		{Ogre::Vector3(9, 0, 9), nullptr},
	};
}

void EnemyManager::update(const Ogre::FrameEvent& fe)
{
	int numOfEnemiesspawned = 0;
	for (std::vector<std::pair<Ogre::Vector3, Enemy*>>::iterator it = enemies.begin(); it != enemies.end(); it++) {
		if (it->second) {
			numOfEnemiesspawned++;
			it->second->getState()->addTime(fe.timeSinceLastFrame);
			if (it->second->getState()->getLength() == it->second->getState()->getTimePosition()) {
				deleteEnemy(it - enemies.begin());
			}
		}
	}
	if (numOfEnemiesspawned == 0) {
		int numberOfNewEnemies = rand() % 3 + 1;
		std::vector<int> vecPos;
		for (int i = 0; i < numberOfNewEnemies; i++) {
			bool isUnique = false;
			int randomPos;
			while (!isUnique)
			{
				randomPos = rand() % 6;
				if (std::find(vecPos.begin(), vecPos.end(), randomPos) == vecPos.end()) {
					isUnique = true;
				}
			}
			vecPos.push_back(randomPos);
		}
		spawnEnemies(vecPos);
	}
}

void EnemyManager::spawnEnemies(const std::vector<int>& positions)
{
	for (int i : positions) {
		enemies.at(i).second = new Enemy(mgr, enemies.at(i).first, i);
		physicsEngine->getCollisionShapes().push_back(enemies.at(i).second->getCollider());
		physicsEngine->getDynamicsWorld()->addRigidBody(enemies.at(i).second->getBody());
	}
}

void EnemyManager::deleteEnemy(const int& pos)
{
	std::vector<std::pair<Ogre::Vector3, Enemy*>>::iterator it = enemies.begin() + pos;
	if (it->second) {
		physicsEngine->getDynamicsWorld()->removeRigidBody(it->second->getBody());
		delete it->second;
		it->second = nullptr;
	}
}

void EnemyManager::deleteAllEnemies()
{
	for (int i = 0; i < enemies.size(); i++) {
		deleteEnemy(i);
	}
}

