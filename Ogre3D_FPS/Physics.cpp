#include "Physics.h"

Physics::~Physics()
{
	for (Ogre::ManualObject* obj : debugLines)
	{
		delete obj;
	}
	debugLines.clear();
}

void Physics::initObjects(Ogre::SceneNode* m)
{
	debuging = false;
	collisionConfiguration = new btDefaultCollisionConfiguration();
	dispatcher = new btCollisionDispatcher(collisionConfiguration);
	overlappingPairCache = new btDbvtBroadphase();
	solver = new btSequentialImpulseConstraintSolver();
	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
	if (debuging) {
		dynamicsWorld->setDebugDrawer(this);
		dynamicsWorld->getDebugDrawer()->setDebugMode(DBG_DrawWireframe);
	}
	phisycsDebuglayer = m;
}

btDiscreteDynamicsWorld* Physics::getDynamicsWorld()
{
	return dynamicsWorld;
}

std::vector<btCollisionShape*> Physics::getCollisionShapes()
{
	return collisionShapes;
}

btCollisionDispatcher* Physics::getDispatcher()
{
	return dispatcher;
}

void Physics::trackRigidBodyWithName(btRigidBody* rb, std::string name)
{
	physicsAccessors.insert(std::pair<std::string, btRigidBody*>(name, rb));
}

int Physics::getCollisionObjectCount()
{
	return physicsAccessors.size();
}

void Physics::drawLine(const btVector3& from, const btVector3& to, const btVector3& toColor)
{
	Ogre::ManualObject* mDragLine = new Ogre::ManualObject("Line" + std::to_string(debugLines.size()));
	mDragLine->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST);
	mDragLine->position(Ogre::Vector3(from.getX(), from.getY(), from.getZ()));
	mDragLine->position(Ogre::Vector3(to.getX(), to.getY(), to.getZ()));
	mDragLine->end();
	Ogre::SceneNode* node = phisycsDebuglayer->createChildSceneNode();
	node->attachObject(mDragLine);
	debugLines.push_back(mDragLine);
}

void Physics::drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color)
{
}

void Physics::reportErrorWarning(const char* warningString)
{
	Ogre::LogManager::getSingletonPtr()->logMessage("*** Bullet Error: " + std::string(warningString) +" ***");
}

void Physics::draw3dText(const btVector3& location, const char* textString)
{
	Ogre::LogManager::getSingletonPtr()->logMessage("*** 3D text: " + std::string(textString) +" * **");
}

void Physics::setDebugMode(int debugMode)
{
	//dynamicsWorld->getDebugDrawer()->setDebugMode(debugMode);//DBG_DrawWireframe);
}

int Physics::getDebugMode() const
{
	//return dynamicsWorld->getDebugDrawer()->getDebugMode();
	return debuging;
}

void Physics::clearDebugLines()
{
	phisycsDebuglayer->detachAllObjects();
	for (Ogre::ManualObject* obj : debugLines)
	{
		delete obj;
	}
	debugLines.clear();
}

std::map<std::string, btRigidBody*>  Physics::getPhisycsAccessors()
{
	return physicsAccessors;
}

