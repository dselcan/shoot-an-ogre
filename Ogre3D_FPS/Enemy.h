/*
Class that takes care of creating and destroying a single enemy
*/

#pragma once
#include <Ogre.h>
#include "BulletCollision/CollisionShapes/btBoxShape.h"
#include "LinearMath/btDefaultMotionState.h"
#include "BulletDynamics/Dynamics/btRigidBody.h"

class Enemy
{
public:
	Enemy(Ogre::SceneManager* mgr, const Ogre::Vector3& pos, const int& index);
	~Enemy();

	void updateAnimation(const Ogre::FrameEvent& fe);

	btCollisionShape* getCollider();
	btRigidBody* getBody();

	Ogre::AnimationState* getState();

	std::string getName();

private:
	Ogre::SceneNode* enemy;
	Ogre::SceneManager* mgr;
	btCollisionShape* collider;
	btRigidBody* body;
	int index;

	Ogre::AnimationState* ogreState;
};

