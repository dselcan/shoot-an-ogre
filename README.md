# Shoot an Ogre

Shoot an ogre is FPS minigame based on the carnival game "Whack a Mole".
The game is created using Ogre3d rendering engine and Bullet phisycs engine.
the project is coded using C++.

## Building
To build this project a bit of setup is required.
The project has three dependencies, that need to be build from source and then linked in the project (Ogre, OIS, Bullet).

1. first we need to generate the visual studio 2019 project from the provided CMAKE files for each project.
  * a. open CMake GUI
  * b. select source directory for dependency (ogre-1.12.12, OIS-1.5.1, bullet3-3.17)
  * c. select output directory (<dependency_dir>/out/build)
  * d. click Configure & select visual studio 2019 on the popup
	    (note: for bullet we need to make sure that before we generate, the variable    	    "USE_MSVC_RUNTIME_LIBRARY_DLL" is checked)
  * e. click Generate.
2. now that the project is ready. we can open it with visual studio & build.
  * a. open project.
  * b. right click on ALL_BUILD and click compile.
3. all our dependencies are built. now we just jhave to copy the generated dll's into the project source file folder.
  * copy from ogre-1.12.12/out/build/bin/release/*.dll to Ogre3D_FPS/Ogre3D_FPS
  * copy from OIS-1.5.1/out/build/Release/*.dll to Ogre3D_FPS/Ogre3D_FPS
4. from here refer to the Simple section of these instructions.

Simple (download realese zip source):
All the libraries are already build and set up where the application expects them, all there is to do is open the solution with visual studio 2019 and run the build:
1. Open visual studio 2019 & click on "Open a project or solution". Navigate to the extracted folder and open Ogre3D_FPS/Ogre3D_FPS.sln
2. when the project loads click on "Local Windows Debugger" or press Ctrl + F5.
3. on the first run OGRE will ask which rendering system you would like to use and offer additional setup options. pick one from the list and optionally edit the resolution.
4. enjoy the game.

## How to run

if you would just like to play the game just go to the Releases section of the repository.
Once there downlaod the latest release download the file ShootAnOgre_x.x.zip, and extract its contents.
After the extraction is complete double-click on <extract_location>/shootAnOgre_x.x/Ogre3D_FPS/Ogre3D_FPS.exe.